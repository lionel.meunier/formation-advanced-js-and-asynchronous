(function () {
    titleLog('Evaluate Exo');

    function verifResult(id, value) {
        const div = document.getElementById(id);
        let result;
        if (id === 'exoVar') {
            result = verifExoVar(value);
        }
        if (id === 'exoTableau') {
            result = verifExoTableau(value);
        }
        if (id === 'exoFunction') {
            result = verifExoFunction(value);
        }
        if (result) {
            console.log(`${id} is success with value`, value);
            div.classList.remove('text-danger');
            div.classList.add('text-success');
        } else {
            console.log(`${id} is error with value`, value);
            div.classList.remove('text-success');
            div.classList.add('text-danger');
        }

    }

    function verifExoVar(value) {
        return typeof value === 'string' && parseInt(value) === 10;
    }


    function verifExoTableau(value) {
        const response = [3, 5, 7, 9];
        return JSON.stringify(value) === JSON.stringify(response);
    }

    function verifExoFunction(value) {
        if (typeof value === 'function') {
            const values = [1, 2, 3, 4, 5];
            const result = values.map(value);
            const response = [10, 20, 30, 40, 50];
            return JSON.stringify(result) === JSON.stringify(response)
        }
        return false;
    }

    function validCode(id) {
        const div = document.getElementById(id);
        const code = div.querySelector('code');
        try {
            eval(code.innerText);
        }catch (e) {
            console.error('ERROR EVAL',e)
        }
    }

    window.validCode = validCode;
    window.verifResult = verifResult;
})();
