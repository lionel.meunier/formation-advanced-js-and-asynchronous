(function () {
    titleLog('Ajax Exo');

    function verifResult(id, value) {
        let result;
        if (id === 'exoPromise') {
            verifExoPromise(value).then((result) => {
                checkResult(id, result);
            });
        }
        checkResult(id, result);

    }

    function verifExoPromise(value) {
        return new Promise((resolve) => {
            try {
                const response = {
                    "userId": 1,
                    "id": 1,
                    "title": "delectus aut autem",
                    "completed": false
                };
                value('https://jsonplaceholder.typicode.com/todos/1').then((val) => {
                    resolve((JSON.stringify(val) === JSON.stringify(response)))
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    function checkResult(id, result) {
        const div = document.getElementById(id);
        if (result) {
            console.log(`${id} is success with value`);
            div.classList.remove('text-danger');
            div.classList.add('text-success');
        } else {
            console.log(`${id} is error with value`);
            div.classList.remove('text-success');
            div.classList.add('text-danger');
        }
    }


    function validCode(id) {
        const div = document.getElementById(id);
        const code = div.querySelector('code');
        try {
            eval(code.innerText);
        } catch (e) {
            console.error('ERROR EVAL', e)
        }
    }

    window.validCode = validCode;
    window.verifResult = verifResult;
})();
